package pl.wiickend.restaurant.views.controllers.warehouse;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import pl.wiickend.restaurant.entity.Product;
import pl.wiickend.restaurant.entity.ProductOrder;
import pl.wiickend.restaurant.entity.ProductOrderStatus;
import pl.wiickend.restaurant.services.entity.ProductOrderService;

import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class WarehouseProductDetailsController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label LabelName;

    @FXML
    private Label LabelIDProduct;

    @FXML
    private Label LabelQuantity;

    @FXML
    private Label LabelUseDate;

    @FXML
    private Label LabelDataOfLastUpdate;

    @FXML
    private TableView<ProductOrder> orderTable;

    @FXML
    private TableColumn<ProductOrder, Date> date;

    @FXML
    private TableColumn<ProductOrder, ProductOrderStatus> status;

    @FXML
    private TableColumn<ProductOrder, Float> amount;

    @FXML
    private TableColumn<ProductOrder, Float> price;

    private Product product;

    public void setProduct(Product product) {
        this.product = product;
        LabelName.setText(product.getName());
        LabelIDProduct.setText(product.getId().toString());
        LabelQuantity.setText(product.getQuantity().toString());
        LabelDataOfLastUpdate.setText(product.getUpdatedAt().toString());
    }

    @FXML
    void backButton(ActionEvent event) {
        Stage stage = (Stage) LabelDataOfLastUpdate.getScene().getWindow();
        stage.close();
    }

    @FXML
    void createNewOrder(ActionEvent event) {
//        todo add logic to open new window to create order
        Dialog<List<String>> dialog = new Dialog<>();
        dialog.setTitle("New Product Order");
        dialog.setHeaderText("Product Order for " + product.getName());

        ButtonType createOrderButton = new ButtonType("Create Product Order", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(createOrderButton, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 20, 20, 20));

        TextField amount = new TextField();
        amount.setPromptText("Amount");

        TextField price = new TextField();
        price.setPromptText("Price");

        DatePicker orderDate = new DatePicker();
        orderDate.setPromptText("Order Date");
        orderDate.setValue(LocalDate.now());

        grid.add(new Label("Unit :"), 0, 0);
        grid.add(new Label(product.getUnit()), 1, 0);

        grid.add(new Label("Amount: "), 0, 1);
        grid.add(amount, 1, 1);

        grid.add(new Label("Order Date: "), 0, 2);
        grid.add(orderDate, 1, 2);

        grid.add(new Label("Price per unit: "), 0, 3);
        grid.add(price, 1, 3);


        Node createProductOrder = dialog.getDialogPane().lookupButton(createOrderButton);
        createProductOrder.setDisable(true);

        amount.textProperty().addListener((observable, oldValue, newValue) -> {
            createProductOrder.setDisable(checkProductOrderDialogInput(price, amount, orderDate));
        });

        price.textProperty().addListener((observable, oldValue, newValue) -> {
            createProductOrder.setDisable(checkProductOrderDialogInput(price, amount, orderDate));
        });

        orderDate.valueProperty().addListener((observable, oldValue, newValue) -> {
            createProductOrder.setDisable(checkProductOrderDialogInput(price, amount, orderDate));
        });

        dialog.getDialogPane().setContent(grid);

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == createOrderButton) {
                List<String> res = new ArrayList<>();
                res.add(amount.getText());
                res.add(price.getText());
                res.add(orderDate.getValue().toString());
                return res;
            }
            return null;
        });
        Optional<List<String>> result = dialog.showAndWait();

        result.ifPresent(resultData -> {
            ProductOrder productOrder = new ProductOrder();
            productOrder.setProduct(product);
            productOrder.setQuantity(Float.parseFloat(resultData.get(0)));
            productOrder.setPrice(Float.parseFloat(resultData.get(1)));
            productOrder.setOrder_date(Date.valueOf(resultData.get(2)));
            productOrder.setProductOrderStatus(ProductOrderStatus.ordered);
            productOrderService.save(productOrder);
        });
        this.updateProductOrders();
    }

    private boolean checkProductOrderDialogInput(TextField price, TextField amount, DatePicker date) {
        boolean inValid = false;
        try {
            Float x = Float.parseFloat(amount.getText());
            Float y = Float.parseFloat(price.getText());
            if (x < 0 || y < 0) {
                inValid = true;
            }
        } catch (Exception e) {
            inValid = true;
        }
        if (date.getValue().compareTo(LocalDate.now()) < 0) {
            inValid = true;
        }
        return inValid;
    }

    @FXML
    void deleteOrder(ActionEvent event) {
        if (orderTable.getSelectionModel().getSelectedItem() == null) {
            return;
        }
        productOrderService.delete(orderTable.getSelectionModel().getSelectedItem());
        this.updateProductOrders();
    }

    @FXML
    void updateOrderStatus(ActionEvent event) {
        if (
                orderTable.getSelectionModel().getSelectedItem() == null ||
                        orderTable.getSelectionModel().getSelectedItem().getProductOrderStatus() != ProductOrderStatus.ordered
        ) {
            return;
        }

        ProductOrder productOrder = orderTable.getSelectionModel().getSelectedItem();

        Dialog<ProductOrderStatus> dialog = new Dialog<>();
        dialog.setTitle("Product Order Status Update");
        dialog.setHeaderText("Product Order for " + product.getName());

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 20, 20, 20));

        grid.add(new Label("Unit"), 0, 0);
        grid.add(new Label(productOrder.getProduct().getUnit()), 1, 0);
        grid.add(new Label("Amount"), 0, 1);
        grid.add(new Label(productOrder.getQuantity().toString()), 1, 1);
        grid.add(new Label("Price"), 0, 2);
        grid.add(new Label(productOrder.getPrice().toString()), 1, 2);

        ChoiceBox<ProductOrderStatus> statusChoicebox = new ChoiceBox<>();
        statusChoicebox.getItems().setAll(ProductOrderStatus.values());
        statusChoicebox.getItems().remove(ProductOrderStatus.ordered);
        statusChoicebox.setValue(productOrder.getProductOrderStatus());


        grid.add(new Label("Status"), 0, 3);
        grid.add(statusChoicebox, 1, 3);

        dialog.getDialogPane().setContent(grid);

        ButtonType updateStatus = new ButtonType("Update Status", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(updateStatus, ButtonType.CANCEL);

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == updateStatus) {
                return statusChoicebox.getSelectionModel().getSelectedItem();
            }
            return null;
        });

        Node updateOrderStatus = dialog.getDialogPane().lookupButton(updateStatus);
        updateOrderStatus.setDisable(true);

        statusChoicebox.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.equals(productOrder.getProductOrderStatus())) {
                updateOrderStatus.setDisable(false);
            }
        });

        Optional<ProductOrderStatus> result = dialog.showAndWait();

        result.ifPresent(resultData -> {
            productOrderService.updateStatus(productOrder, resultData);
        });
        this.updateProductOrders();
    }

    private ProductOrderService productOrderService;
    private List<ProductOrder> productOrders;

    @FXML
    void initialize() {
        assert LabelName != null : "fx:id=\"LabelName\" was not injected: check your FXML file 'WarehouseProductDetailsView.fxml'.";
        assert LabelIDProduct != null : "fx:id=\"LabelIDProduct\" was not injected: check your FXML file 'WarehouseProductDetailsView.fxml'.";
        assert LabelQuantity != null : "fx:id=\"LabelQuantity\" was not injected: check your FXML file 'WarehouseProductDetailsView.fxml'.";
        assert LabelUseDate != null : "fx:id=\"LabelUseDate\" was not injected: check your FXML file 'WarehouseProductDetailsView.fxml'.";
        assert LabelDataOfLastUpdate != null : "fx:id=\"LabelDataOfLastUpdate\" was not injected: check your FXML file 'WarehouseProductDetailsView.fxml'.";
        productOrderService = new ProductOrderService();

        date.setCellValueFactory(new PropertyValueFactory<ProductOrder, Date>("order_date"));
        price.setCellValueFactory(new PropertyValueFactory<ProductOrder, Float>("price"));
        amount.setCellValueFactory(new PropertyValueFactory<ProductOrder, Float>("quantity"));
        status.setCellValueFactory(new PropertyValueFactory<ProductOrder, ProductOrderStatus>("productOrderStatus"));

        this.updateProductOrders();
    }

    private void updateProductOrders() {
        productOrders = productOrderService.loadAll();
        this.updateTable();
    }

    private void updateTable() {
        orderTable.getItems().clear();
        orderTable.setItems((ObservableList<ProductOrder>) FXCollections.observableList(productOrders));
    }
}
