package pl.wiickend.restaurant.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity(name = "MealMenu")
@Table(name = "menus_meals")
public class MealMenu {
 
    @EmbeddedId
    private MealMenuId id;
 
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("mealId")
    private Meal meal;
 
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("menuId")
    private Menu menu;
 
    @CreationTimestamp
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

    private Float promotion;

    public MealMenu() {

    }

    public MealMenu(Meal meal, Menu menu) {
        this.meal = meal;
        this.menu = menu;
        this.id = new MealMenuId(meal.getId(), menu.getId());
    }

    //Getters and setters omitted for brevity

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//
//        if (o == null || getClass() != o.getClass())
//            return false;
//
//        MealMenu that = (MealMenu) o;
//        return Objects.equals(meal, that.meal) &&
//               Objects.equals(menu, that.menu);
//    }

    @Override
    public int hashCode() {
        return Objects.hash(meal, menu);
    }

    public Meal getMeal() {
        return meal;
    }

    public void setMeal(Meal meal) {
        this.meal = meal;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public Float getPromotion() {
        return promotion;
    }

    public String getMealName() {
        return meal.getName();
    }

    public Float getMealPrice() {
        return meal.getGross_price();
    }

    public void setPromotion(Float promotion) {
        this.promotion = promotion;
    }


}
