package pl.wiickend.restaurant.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import pl.wiickend.restaurant.services.entity.MealMenuService;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Entity(name="Meal")
@Table(name="meals")
public class Meal implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(unique=true)
    String name;
    String description;
    String calories;
    Float net_price;
    Float gross_price;
    Float average_preparation_time;
    String recipe;
    @CreationTimestamp
    private Date createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

    @Transient
    Float inOrderQuantity;
    
    @OneToMany(
        mappedBy = "meal",
        cascade = CascadeType.ALL,
        orphanRemoval = true
    )
    private List<MealProduct> products = new ArrayList();

    @Transient
    private Float promotion = gross_price;

    @OneToMany(
        mappedBy = "meal",
        cascade = CascadeType.ALL,
        orphanRemoval = true
    )
    private List<MealMenu> menus;

    @OneToMany(
            mappedBy = "meal",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<MealOrder> orders;

    public Meal() {
    }

    public Meal(String description, String calories, Float net_price, Float gross_price, Float average_preparation_time, String recipe, List<Product> products) {
        this.description = description;
        this.calories = calories;
        this.net_price = net_price;
        this.gross_price = gross_price;
        this.average_preparation_time = average_preparation_time;
        this.recipe = recipe;
    }

    public Meal(MealMenu mealMenu) {
        Meal x = mealMenu.getMeal();
        this.id = x.getId();
        this.name = x.getName();
        this.description = x.getDescription();
        this.calories = x.getCalories();
        this.net_price = x.getNet_price();
        this.gross_price = x.getNet_price();
        this.promotion = x.getPromotion();
        this.average_preparation_time = x.getAverage_preparation_time();
        this.recipe = x.getRecipe();
        this.products = x.getProducts();

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCalories() {
        return calories;
    }

    public void setCalories(String calories) {
        this.calories = calories;
    }

    public Float getNet_price() {
        return net_price;
    }

    public void setNet_price(Float net_price) {
        this.net_price = net_price;
    }

    public Float getGross_price() {
        return gross_price;
    }

    public void setGross_price(Float gross_price) {
        this.gross_price = gross_price;
    }

    public Float getAverage_preparation_time() {
        return average_preparation_time;
    }

    public void setAverage_preparation_time(Float average_preparation_time) {
        this.average_preparation_time = average_preparation_time;
    }

    public String getRecipe() {
        return recipe;
    }

    public void setRecipe(String recipe) {
        this.recipe = recipe;
    }

    public Date getCreated_at() {
        return createdAt;
    }

    public void setCreated_at(Date created_at) {
        this.createdAt = created_at;
    }

    public LocalDateTime getUpdated_at() {
        return updatedAt;
    }

    public void setUpdated_at(Date updated_at) {
        this.updatedAt = updatedAt;
    }

    public List<MealProduct> getProducts() {
        return products;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void setProducts(List<MealProduct> products) {
        this.products = products;
    }

    public Float getInOrderQuantity() {
        return inOrderQuantity;
    }

    public void setInOrderQuantity(Float inOrderQuantity) {
        this.inOrderQuantity = inOrderQuantity;
    }

    public void addProduct(Product product) {
        MealProduct mealProduct = new MealProduct(this, product);
        products.add(mealProduct);
        product.getMeals().add(mealProduct);
    }

    public void removeProduct(Product product) {
        for (Iterator<MealProduct> iterator = products.iterator();
             iterator.hasNext(); ) {
            MealProduct postProduct = iterator.next();

            if (postProduct.getMeal().equals(this) &&
                    postProduct.getProduct().equals(product)) {
                iterator.remove();
                postProduct.getProduct().getMeals().remove(postProduct);
                postProduct.setMeal(null);
                postProduct.setProduct(null);
            }
        }
    }

    public List<MealOrder> getOrders() {
        return orders;
    }

    public void setOrders(List<MealOrder> orders) {
        this.orders = orders;
    }

    @Override
    public String toString() {
        return "Meal{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", calories='" + calories + '\'' +
                ", net_price=" + net_price +
                ", gross_price=" + gross_price +
                ", average_preparation_time=" + average_preparation_time +
                ", recipe='" + recipe + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", inOrderQuantity=" + inOrderQuantity +
                ", products=" + products +
                ", promotion=" + promotion +
                ", menus=" + menus +
                ", orders=" + orders +
                '}';
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
 
        Meal meal = (Meal) o;

        return this.getId() == meal.getId();
    }

    public List<MealMenu> getMenus() {
        return menus;
    }

    public void setMenus(List<MealMenu> menus) {
        this.menus = menus;
    }

    public Float getPromotion() {
        return promotion;
    }

    public void setPromotion(Float promotion) {
        this.promotion = promotion;
    }

    public void setPromotionInRelation(Menu menu, Float promotion) {
        for (Iterator<MealMenu> iterator = menus.iterator();
             iterator.hasNext(); ) {
                MealMenu mealMenu = iterator.next();
 
            if (mealMenu.getMeal().equals(this) &&
                    mealMenu.getMenu().equals(menu)) {
                mealMenu.setPromotion(promotion);
                MealMenuService mealMenuService = new MealMenuService();
                mealMenuService.update(mealMenu);
            }
        }
    }

    public Float getPromotionFromRelation(Menu menu) {
        for (Iterator<MealMenu> iterator = menus.iterator();
             iterator.hasNext(); ) {
                MealMenu mealMenu = iterator.next();
 
            if (mealMenu.getMeal().equals(this) &&
                    mealMenu.getMenu().equals(menu)) {
                return mealMenu.getPromotion();
            }
        }

        return gross_price;
    }
}
