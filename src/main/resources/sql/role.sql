-- Clear table
TRUNCATE TABLE `roles`;
-- Import data
INSERT INTO `roles` (`id`, `name`) VALUES (1,'Manager');
INSERT INTO `roles` (`id`, `name`) VALUES (2,'Chef');
INSERT INTO `roles` (`id`, `name`) VALUES (3,'Waiter');
INSERT INTO `roles` (`id`, `name`) VALUES (4,'Warehouseman');
