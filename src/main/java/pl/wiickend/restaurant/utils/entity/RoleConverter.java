package pl.wiickend.restaurant.utils.entity;

import javafx.util.StringConverter;
import pl.wiickend.restaurant.entity.Role;

import java.util.HashMap;
import java.util.Map;

public class RoleConverter extends StringConverter<Role> {

    /** Cache of Products */
    private Map<String, Role> roleMap = new HashMap<String, Role>();

    @Override
    public String toString(Role role) {
        roleMap.put(role.getName(), role);
        return role.getName();
    }

    @Override
    public Role fromString(String name) {
        return roleMap.get(name);
    }

}