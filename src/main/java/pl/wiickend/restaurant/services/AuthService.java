package pl.wiickend.restaurant.services;

import pl.wiickend.restaurant.entity.User;
import pl.wiickend.restaurant.Main;
import pl.wiickend.restaurant.utils.Database;

public class AuthService {
    public boolean login(String username, String password) throws Exception {

        Main.database = new Database();
        Main.database.openCurrentSessionWithTransaction();

        User user = Main.database.getCurrentSession().createQuery(
                "SELECT u FROM User u WHERE u.username = :custName AND u.password = :password", User.class)
                .setParameter("custName", username)
                .setParameter("password", password)
                .setMaxResults(1)
                .getSingleResult();

        Main.database.closeCurrentSessionWithTransaction();

        if (user == null) {
            throw new Exception("No such user found.");
        } else {
            Main.loggedUser = user;
            return true;
        }
    }
}
