package pl.wiickend.restaurant.services.entity;

import pl.wiickend.restaurant.entity.*;
import pl.wiickend.restaurant.Main;
import pl.wiickend.restaurant.utils.Dao;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MealOrderService extends Dao<MealOrder> {
    public List<MealOrder> getMealOrdersWithStatus(MealOrderStatus x) {
        try {
            Main.database.openCurrentSessionWithTransaction();
            List<MealOrder> mealOrders = Main.database.getCurrentSession()
                    .createQuery("SELECT x FROM MealOrder x WHERE mealOrderStatus = :status", MealOrder.class).setParameter("status", x)
                    .getResultList();
            Main.database.closeCurrentSessionWithTransaction();
            return mealOrders;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return new ArrayList<MealOrder>();
    }

    public MealOrder getByMealOrder(Meal meal, Order order) {
        try {
            Main.database.openCurrentSessionWithTransaction();
            MealOrder mealOrder = Main.database.getCurrentSession()
                    .createQuery("SELECT x FROM MealOrder x WHERE x.meal = :meal AND x.order = :order", MealOrder.class)
                    .setParameter("meal", meal)
                    .setParameter("order", order)
                    .getSingleResult();
            Main.database.closeCurrentSessionWithTransaction();
            return mealOrder;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return new MealOrder();
    }

    public void changeMealOrderStateToPreparation(MealOrder mealOrder, User chef) throws Exception {
        Main.database.openCurrentSessionWithTransaction();

        mealOrder.setChef(chef);
        mealOrder.setMealOrderStatus(MealOrderStatus.preparing);
        mealOrder.setPreparationStart(LocalDateTime.now());

        boolean enoughProducts = true;

        StringBuilder sb = new StringBuilder("Not enough products" + System.lineSeparator());

        for (MealProduct x : mealOrder.getMeal().getProducts()) {
            sb.append(x.getProduct().getName() + "  needed: " + x.getQuantity() * mealOrder.getQuantity() + ", in warehouse: " + x.getProduct().getQuantity());
            if (x.getProduct().getQuantity() < x.getQuantity() * mealOrder.getQuantity()) {
                enoughProducts = false;
                sb.append(" = Not Enough");
            } else {
                sb.append(" = Enough");
            }
            sb.append(System.lineSeparator());
        }

        if (!enoughProducts) {
            throw new Exception(sb.toString());
        }

        for (MealProduct x : mealOrder.getMeal().getProducts()) {
            Product product = Main.database.getCurrentSession().get(Product.class, x.getProduct().getId());
            product.setQuantity(x.getProduct().getQuantity() - (x.getQuantity() * mealOrder.getQuantity()));
            Main.database.getCurrentSession().update(x);
        }

        Main.database.getCurrentSession().update(mealOrder);

        Main.database.closeCurrentSessionWithTransaction();
    }

    public void changeMealOrderStateToDone(MealOrder mealOrder) {
        Main.database.openCurrentSessionWithTransaction();
        mealOrder.setPreparationEnd(LocalDateTime.now());
        mealOrder.setMealOrderStatus(MealOrderStatus.done);
        Main.database.getCurrentSession().update(mealOrder);
        Main.database.closeCurrentSessionWithTransaction();
    }
}
