package pl.wiickend.restaurant.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name="menus")
public class Menu implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(unique = true)
    String name;

    @Column(name = "description")
    String description;

    @OneToMany(
        mappedBy = "menu",
        cascade = CascadeType.ALL,
        orphanRemoval = true
    )
    private List<MealMenu> meals;

    private boolean active;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @CreationTimestamp
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

    public Menu() {

    }

    public Long getId() {
        return id;
    }

    public Menu(String name, String description, Boolean active) {
        this.name = name;
        this.description = description;
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addMeal(Meal meal) {
        MealMenu mealMenu = new MealMenu(meal, this);
        meals.add(mealMenu);
        meal.getMenus().add(mealMenu);
    }

    public void addMealMenu(MealMenu mealMenu) {
        meals.add(mealMenu);
    }

    public void removeMealMenu(MealMenu mealMenu) {
        meals.remove(mealMenu);
    }

//    public void removeMeal(Meal meal) {
//        for (Iterator<MealMenu> iterator = meals.iterator();
//             iterator.hasNext(); ) {
//            MealMenu mealMenu = iterator.next();
//
//            if (mealMenu.getMenu().equals(this) &&
//                    mealMenu.getMeal().equals(meal)) {
//                iterator.remove();
//                mealMenu.getMenu().getMeals().remove(mealMenu);
//                mealMenu.setMeal(null);
//                mealMenu.setMenu(null);
//            }
//        }
//    }

    public List<MealMenu> getMeals() {
        return meals;
    }

    public void setMeals(List<MealMenu> meals) {
        this.meals = meals;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
 
        Menu menu = (Menu) o;
        return Objects.equals(getId(), menu.getId());
    }

}
