package pl.wiickend.restaurant.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.NaturalIdCache;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity(name="Order")
@Table(name="orders")

@NaturalIdCache
public class Order implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    Integer table_no;
    Float total;

    @CreationTimestamp
    private Date createdAt;

    @UpdateTimestamp
    private Date updatedAt;

    @OneToMany(
            mappedBy = "order",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    List<MealOrder> meals = new ArrayList<>();

    public Order() {
    }

    public Order(Integer net_price, Integer gross_price, Date pickup_time, Date time_in_kitchen, Float total, List<MealOrder> meals) {
        this.total = total;
        this.meals = meals;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getTable_no() {
        return table_no;
    }

    public void setTable_no(Integer table_no) {
        this.table_no = table_no;
    }

    public List<MealOrder> getMeals() {
        return meals;
    }

    public void setMeals(List<MealOrder> meals) {
        this.meals = meals;
    }

    public void addMeal(Meal meal) {
        MealOrder mealOrder = new MealOrder(meal, this);
        mealOrder.setMealOrderStatus(MealOrderStatus.newMeal);
        meals.add(mealOrder);
        meal.getOrders().add(mealOrder);
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", table_no=" + table_no +
                ", total=" + total +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", meals=" + meals +
                '}';
    }
}
