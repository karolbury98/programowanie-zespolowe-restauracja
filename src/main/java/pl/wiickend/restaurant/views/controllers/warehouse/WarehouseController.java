package pl.wiickend.restaurant.views.controllers.warehouse;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.wiickend.restaurant.entity.Product;
import pl.wiickend.restaurant.services.entity.ProductService;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.function.Predicate;

public class WarehouseController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label title;

    @FXML
    private TableView<Product> tableProducts;

    @FXML
    private TableColumn<Product, Long> Id;

    @FXML
    private TableColumn<Product, String> name;

    @FXML
    private TableColumn<Product, Float> quantity;

    @FXML
    private TableColumn<Product, String> description;

    @FXML
    private TextField search;

    @FXML
    private Button edit_btn;

    @FXML
    private Button del_btn;

    @FXML
    private Button show_btn;

    private ProductService productService;

    private List<Product> products;

    @FXML
    void newProduct(ActionEvent event) throws Exception {
        URL url = getClass().getResource("/fxml/warehouse/WarehouseNewProductView.fxml");
        FXMLLoader loader = new FXMLLoader(url);
        if(loader == null)
        {
            throw new RuntimeException ("Could not find: "+url.toString());
        }
        Parent root = loader.load();
        WarehouseNewProductController controller =(WarehouseNewProductController) loader.getController();
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner((Stage)search.getScene().getWindow());
        stage.setTitle("New product");
        stage.setScene(scene);
        stage.showAndWait();
        this.updateProducts();
    }

    @FXML
    void deleteButton(ActionEvent event) throws Exception {

        if(tableProducts.getSelectionModel().getSelectedItem()== null)
        {
            return;
        }

        // todo this button should delete product from warehouse
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText("Look, a Confirmation Dialog");
        alert.setContentText("Are you ok with this?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            productService.delete(tableProducts.getSelectionModel().getSelectedItem());
            this.updateProducts();
        }
    }

    @FXML
    void editButton(ActionEvent event) throws IOException {
        if (tableProducts.getSelectionModel().getSelectedItem() == null) {
            return;
        }
        URL url = getClass().getResource("/fxml/warehouse/WarehouseEditProductView.fxml");
        FXMLLoader loader = new FXMLLoader(url);
        if (loader == null) {
            throw new RuntimeException("Could not find: " + url.toString());
        }
        Parent root = loader.load();
        WarehouseEditProductController controller = (WarehouseEditProductController) loader.getController();
        controller.setProduct(tableProducts.getSelectionModel().getSelectedItem());
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner((Stage) edit_btn.getScene().getWindow());
        stage.setTitle("Edit product");
        stage.setScene(scene);
        stage.showAndWait();
        this.updateProducts();
    }

    @FXML
    void showButton(ActionEvent event) throws IOException {
        if (tableProducts.getSelectionModel().getSelectedItem() == null) {
            return;
        }
        URL url = getClass().getResource("/fxml/warehouse/WarehouseProductDetailsView.fxml");
        FXMLLoader loader = new FXMLLoader(url);
        if (loader == null) {
            throw new RuntimeException("Could not find: " + url.toString());
        }
        Parent root = loader.load();
        WarehouseProductDetailsController controller = (WarehouseProductDetailsController) loader.getController();
        controller.setProduct(tableProducts.getSelectionModel().getSelectedItem());
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner((Stage) show_btn.getScene().getWindow());
        stage.setTitle("New order");
        stage.setScene(scene);
        stage.showAndWait();
        this.updateProducts();
    }

    @FXML
    void correctProduct(ActionEvent event) {
        if (tableProducts.getSelectionModel().getSelectedItem() == null)
            return;
        Product selectedProduct = tableProducts.getSelectionModel().getSelectedItem();
        TextInputDialog dialog = new TextInputDialog(selectedProduct.getQuantity().toString());
        dialog.setHeaderText("Product Correction");
        dialog.setContentText("Enter new quantity:");

// Traditional way to get the response value.
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            boolean error = false;
            try {
                selectedProduct.setQuantity(Float.valueOf(result.get()));
            } catch (Exception e) {
                error = true;
            }
            if (error)
                return;
            selectedProduct.setQuantity(Float.valueOf(result.get()));
            productService.update(selectedProduct);
        }
        this.updateProducts();
    }

    @FXML
    public void initialize() throws Exception {
        assert title != null : "fx:id=\"title\" was not injected: check your FXML file 'WarehouseView.fxml'.";
        assert edit_btn != null : "fx:id=\"edit_btn\" was not injected: check your FXML file 'WarehouseView.fxml'.";
        assert del_btn != null : "fx:id=\"del_btn\" was not injected: check your FXML file 'WarehouseView.fxml'.";
        assert show_btn != null : "fx:id=\"show_btn\" was not injected: check your FXML file 'WarehouseView.fxml'.";
        Id.setCellValueFactory(new PropertyValueFactory<Product, Long>("id"));
        name.setCellValueFactory(new PropertyValueFactory<Product, String>("name"));
        quantity.setCellValueFactory(new PropertyValueFactory<Product, Float>("quantity"));
        description.setCellValueFactory(new PropertyValueFactory<Product, String>("description"));
        productService = new ProductService();
        this.updateProducts();
    }

    private void updateProducts() {
//        products= productService.findAll();
        products = productService.loadAll();
        this.updateTable();
    }

    private void updateTable() {
//        tableProducts.getItems().clear();
        SortedList<Product> productSortedList = sortedList(products);
        tableProducts.setItems(productSortedList);
    }

    private SortedList<Product> sortedList(List<Product> products)
    {
        ObservableList<Product> productObservableList = (ObservableList<Product>) FXCollections.observableList(products);

        FilteredList<Product> productFilteredList = new FilteredList<Product>(productObservableList);

        Predicate predicate = new Predicate() {
            @Override
            public boolean test(Object o) {
                Product product = new Product((Product)o);

                String searchString = search.getText().toLowerCase();

                if(product.getName().toLowerCase().contains(searchString))
                {
                    return  true;
                }else if(product.getDescription().toLowerCase().contains(searchString))
                {
                    return true;
                }
                return false;
            }
        };

        productFilteredList.setPredicate(predicate);

        SortedList<Product> sortedList = new SortedList<>(productFilteredList);
        sortedList.comparatorProperty().bind(tableProducts.comparatorProperty());

        return sortedList;
    }

    public void filterTableView(KeyEvent keyEvent) throws Exception {
        this.updateTable();
    }
}

