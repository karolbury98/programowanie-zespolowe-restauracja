--
-- Zrzut danych tabeli `products`
--
TRUNCATE TABLE `products`;

INSERT INTO `products` (`id`, `name`, `unit`, `quantity`, `description`, `net_price`, `gross_price`, `createdAt`, `updatedAt`)
VALUES
(1, 'Potatoes', 'kg', 5, 'Good potatoes from polish village.', 2.00, 2.46, '2021-05-03', '2021-05-03'),
(2, 'Tomatoes', 'kg', 3, 'Good tomatoes from polish village.', 2.00, 2.46, '2021-05-03', '2021-05-03'),
(3, 'Onion', 'kg', 2, 'Onion', 2.00, 2.46, '2021-07-07', '2021-05-03'),
(4, 'Pasta', 'packages', 20, 'The best pasta from Italy.', 12.00, 12.46, '2021-05-03', '2021-05-03'),
(5, 'Butter', 'packages', 10, 'Butter.', 12.00, 12.46, '2021-07-07', '2021-05-03'),
(6, 'Sugar', 'kg', 25, 'Sugar is sweet.', 12.00, 12.46, '2021-07-07', '2021-05-03'),
(7, 'Salt', 'kg', 12, 'Salt is salty.', 12.00, 12.46, '2021-07-07', '2021-05-03'),
(8, 'Pepper', 'kg', 2, 'Pepper', 12.00, 12.46, '2021-07-07', '2021-05-03'),
(9, 'Red paprika', 'piece', 50, 'Red paprika.', 12.00, 12.46, '2021-07-07', '2021-05-03'),
(10, 'Yellow paprika', 'piece', 77, 'Yellow paprika.', 12.00, 12.46, '2021-05-03', '2021-05-03'),
(11, 'Green paprika', 'piece', 33, 'Green paprika.', 12.00, 12.46, '2021-05-03', '2021-05-03'),
(12, 'Lettuce', 'piece', 10, 'Lettuce.', 12.00, 12.46, '2021-07-07', '2021-05-03'),
(13, 'Peanuts', 'kg', 5, 'peanuts.', 12.00, 12.46, '2021-07-07', '2021-05-03'),
(14, 'Hazelnuts', 'kg', 8, 'hazelnuts.', 12.00, 12.46, '2021-05-03', '2021-05-03'),
(15, 'Cucumber', 'kg', 3, 'Cucumber from polish village.', 12.00, 12.46, '2021-05-03', '2021-05-03'),
(16, 'Pumpkin', 'kg', 12, 'Pumpkin.', 12.00, 12.46, '2021-07-07', '2021-05-03'),
(17, 'Courgette', 'kg', 23, 'Courgette.', 12.00, 12.46, '2021-07-07', '2021-05-03'),
(18, 'Ketchup', 'packages', 5, 'Ketchup pudliszki.', 12.00, 12.46, '2021-07-07', '2021-05-03'),
(19, 'Mustard', 'packages', 8, 'Mustard roleski.', 12.00, 12.46, '2022-07-07', '2021-05-03'),
(20, 'Vinegar', 'bottles', 33, 'Vinegar.', 12.00, 12.46, '2021-07-23', '2021-05-03'),
(21, 'Red wine', 'bottles', 150, '20 years old french red wine.', 12.00, 12.46, '2021-07-23', '2021-05-03'),
(22, 'White wine', 'bottles', 120, '12 years old portugal white wine.', 12.00, 12.46, '2021-05-03', '2021-05-03');

TRUNCATE TABLE `products_orders`;

INSERT INTO `products_orders` (`id`, `created_at`, `order_date`, `price`, `productOrderStatus`, `quantity`,
                               `updated_at`, `product_id`)
VALUES (1, '2021-05-07', '2021-05-20', 2, 'ordered', 12, '2021-05-07', 1),
       (2, '2021-05-07', '2021-05-20', 3, 'done', 10, '2021-05-07', 1),
       (3, '2021-05-07', '2021-05-20', 4, 'ordered', 612, '2021-05-07', 2),
       (4, '2021-05-07', '2021-05-20', 5, 'done', 410, '2021-05-07', 2),
       (5, '2021-05-07', '2021-05-20', 6, 'ordered', 312, '2021-05-07', 3),
       (6, '2021-05-07', '2021-05-20', 1, 'done', 110, '2021-05-07', 3),
       (7, '2021-05-07', '2021-05-20', 1.2, 'ordered', 812, '2021-05-07', 4),
       (8, '2021-05-07', '2021-05-20', 1.1, 'done', 210, '2021-05-07', 4),
       (9, '2021-05-07', '2021-05-20', 3, 'ordered', 712, '2021-05-07', 5),
       (10, '2021-05-07', '2021-05-20', 2.7, 'done', 610, '2021-05-07', 5),
       (11, '2021-05-07', '2021-05-20', 2.9, 'ordered', 412, '2021-05-07', 6),
       (12, '2021-05-07', '2021-05-20', 2, 'done', 810, '2021-05-07', 6),
       (13, '2021-05-07', '2021-05-20', 5, 'ordered', 912, '2021-05-07', 7),
       (14, '2021-05-07', '2021-05-20', 7.1, 'done', 120, '2021-05-07', 7),
       (15, '2021-05-07', '2021-05-20', 2.2, 'ordered', 102, '2021-05-07', 8),
       (16, '2021-05-07', '2021-05-20', 2.5, 'done', 100, '2021-05-07', 8),
       (17, '2021-05-07', '2021-05-20', 2.2, 'ordered', 2, '2021-05-07', 9),
       (18, '2021-05-07', '2021-05-20', 2.4, 'done', 1, '2021-05-07', 9);
