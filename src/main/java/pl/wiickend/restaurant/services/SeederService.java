package pl.wiickend.restaurant.services;

import org.hibernate.Session;

import pl.wiickend.restaurant.Main;
import pl.wiickend.restaurant.utils.Database;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;

public class SeederService {

    private static Database database = null;

    public SeederService() throws Exception {
        if(database==null)
        {
            database = new Database();
        }
    }

    /**
     * Seed a Class from a file in "src/sample/resources/sql/{className}.sql",
     *
     * @param type       - which class sql file should be executed
     */
    public void seed(Class type){
        String className = type.getSimpleName().toLowerCase();

        String fileName = "src/main/resources/sql/" + className;

        this.executeSql(fileName);
    }

    /**
     * Execute sql file from route "src/sample/resources/sql/{fileName}.sql"
     * @param fileName - file name to import
     */
    public void executeSql(String fileName){
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(fileName + ".sql"));
            database.openCurrentSessionWithTransaction();
            Session currentSession = database.getCurrentSession();
            String line = reader.readLine();
            StringBuilder query = new StringBuilder();

            while (line != null) {
                // if empty line skip or if comment line skip
                if (line.length() == 0 || (line.length() > 3 && !line.substring(0, 3).equals("-- "))) {
                    query.append(line);

                    // if at end of line "values" add space before next line
                    if (line.length() > 6 && line.substring(line.length() - 6, line.length()).toLowerCase().equals("values"))
                    {
                        query.append(" ");
                    }

                    if (query.toString().indexOf(';') > 0)
                    {
                        // does contain query
                        int semicolonIndex = query.toString().indexOf(';') + 1;
                        String databaseQuery = query.substring(0, semicolonIndex);

                        // update database
                        currentSession.createSQLQuery(databaseQuery).executeUpdate();

                        query = new StringBuilder(query.substring(semicolonIndex, query.length()));
                    }
                }
                // read next line
                line = reader.readLine();
            }

            // commit updates
            database.closeCurrentSessionWithTransaction();

            reader.close();

        }catch (FileNotFoundException e){
            e.printStackTrace();
            Main.logger.log(Level.SEVERE,"Seeder Error."+System.lineSeparator()+"File not found \""+ fileName+"\".");
        } catch (IOException e) {
            e.printStackTrace();
            Main.logger.log(Level.SEVERE,"Seeder Error."+System.lineSeparator()+"Error reading a file \""+ fileName+"\".");
        }catch (Exception e) {
            e.printStackTrace();
            Main.logger.log(Level.SEVERE,"Seeder Error.");
        }
    }
}
