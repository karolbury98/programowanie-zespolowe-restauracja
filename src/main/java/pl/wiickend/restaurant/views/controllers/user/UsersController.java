package pl.wiickend.restaurant.views.controllers.user;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.wiickend.restaurant.entity.Role;
import pl.wiickend.restaurant.entity.User;
import pl.wiickend.restaurant.services.entity.RoleService;
import pl.wiickend.restaurant.services.entity.UserService;
import pl.wiickend.restaurant.utils.AlertAdder;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;

public class UsersController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TableView<User> userTable;

    @FXML
    private TableColumn<User, String> name_col;

    @FXML
    private TableColumn<User, String> surname_col;

    @FXML
    private TableColumn<User, String> username_col;

    @FXML
    private TableColumn<User, String> role_col;

    @FXML
    private TextField userName;

    @FXML
    private TextField userSurname;

    @FXML
    private TextField userUsername;

    @FXML
    private PasswordField userPassword;

    @FXML
    private DatePicker userDateOfBirth;

    @FXML
    private TextField userAddress;

    @FXML
    private ComboBox<String> userRole;

    @FXML
    private TextField filterField;


    User selectedUser;
    Long selectedId;
    ObservableList<User> data = FXCollections.observableArrayList();

    UserService userService = new UserService();
    RoleService roleService = new RoleService();

    public void refreshTable() throws Exception {
        data.clear();

        List<User> usersList = userService.loadAll();
        data = FXCollections.observableList(usersList);
        userTable.setItems(data);
    }

    //todo filtrowanie

    @FXML
    void createUser(ActionEvent event) throws Exception {
        String username = userUsername.getText();
        String surname = userSurname.getText();
        String address = userAddress.getText();
        String password = userPassword.getText();
        LocalDate date = userDateOfBirth.getValue();
        String role = userRole.getValue();

        if(!username.isEmpty() && !surname.isEmpty() && !address.isEmpty() && !password.isEmpty() && userDateOfBirth != null && !role.isEmpty()) {
            Date formatedDate = Date.valueOf(date);
            Long roleId = null;
            List<Role> roles = roleService.loadAll();
            for (int i = 0; i < roles.size(); i++) {
                if (roles.get(i).getName().equals(role)) {
                    roleId = roles.get(i).getId();
                    break;
                }
            }

            Role foundRole = roleService.getById(roleId);
            User user = new User(
                    username,
                    surname,
                    address,
                    password,
                    formatedDate,
                    address,
                    foundRole);

            userService.save(user);

            AlertAdder alert = new AlertAdder(Alert.AlertType.INFORMATION, "User Added", "User successfully added!");
            refreshTable();
            System.out.println("User added!");
        } else{
            AlertAdder alertError = new AlertAdder(Alert.AlertType.ERROR, "Error", "Invalid inputs!");
            System.out.println("Error!");
        }
    }

    @FXML
    void editUser(ActionEvent event) throws Exception {

        if(userTable.getSelectionModel().getSelectedItem()==null)
            return;

        User userToEdit = userTable.getSelectionModel().getSelectedItem();

        URL url=getClass().getResource("/fxml/user/UsersEditView.fxml");
        FXMLLoader loader = new FXMLLoader(url);
        if(loader == null)
        {
            throw new RuntimeException ("Could not find: "+url.toString());
        }
        Parent root = loader.load();
        UserEditController controller =(UserEditController) loader.getController();
        controller.setUser(userToEdit);
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner((Stage)userTable.getScene().getWindow());
        stage.setTitle("Edit user");
        stage.setScene(scene);
        stage.showAndWait();
        refreshTable();
    }

    @FXML
    void showUserDetails(ActionEvent event) throws IOException {
        if(userTable.getSelectionModel().getSelectedItem() != null) {
            URL url = getClass().getResource("/fxml/user/UsersDetailsView.fxml");
            FXMLLoader loader = new FXMLLoader(url);
            if (loader == null) {
                throw new RuntimeException("Could not find: " + url.toString());
            }
            Parent root = loader.load();
            UserDetailsController controller = (UserDetailsController) loader.getController();
            controller.setUser(userTable.getSelectionModel().getSelectedItem());
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner((Stage) userTable.getScene().getWindow());
            stage.setTitle("User details");
            stage.setScene(scene);
            stage.showAndWait();
        } else{
            AlertAdder alertError = new AlertAdder(Alert.AlertType.ERROR, "Error", "Select user!");
            System.out.println("Error! Select user!");
        }
    }

    @FXML
    public void initialize() throws Exception {
        userRole.getItems().removeAll(userRole.getItems());
        userRole.getItems().addAll("Chef", "Waiter", "Warehouseman", "Manager");
        //each cellValueFactory has been set according to the member variables of your entity class
        username_col.setCellValueFactory(new PropertyValueFactory<User, String>("username"));
        surname_col.setCellValueFactory(new PropertyValueFactory<User, String>("surname"));
        role_col.setCellValueFactory(new PropertyValueFactory<User, String>("role"));
        name_col.setCellValueFactory(new PropertyValueFactory<User, String>("name"));
//        userTable.setItems(getUser());

//        userTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
//            if (newSelection != null) {
//                selectedUser = userTable.getSelectionModel().getSelectedItem();
//                selectedId = selectedUser.getId();
//            }
//        });

        data = getUser();
        FilteredList<User> filteredData = new FilteredList<>(data, p -> true);
        filterField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(person -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                if (person.getName().toLowerCase().contains(newValue.toLowerCase())) {
                    return true;
                } else if (person.getSurname().toLowerCase().contains(newValue.toLowerCase())) {
                    return true;
                }
                return false;
            });
        });

        SortedList<User> sortedData = new SortedList<>(filteredData);
        sortedData.comparatorProperty().bind(userTable.comparatorProperty());
        userTable.setItems(sortedData);

    }
    
    public ObservableList<User> getUser() throws Exception {
        ObservableList<User> UserList = FXCollections.observableArrayList();
        List<User> eList = userService.loadAll();
        for (User ent : eList) {
            UserList.add(ent);
        }
        return UserList;
    }

}
