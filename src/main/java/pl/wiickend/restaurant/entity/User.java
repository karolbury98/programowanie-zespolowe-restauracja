package pl.wiickend.restaurant.entity;

import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Entity
@Table(name="users")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String name;
    String surname;

    @Column(unique = true)
    String username;
    String password;
    Date data_of_birth;
    String address;

    @ColumnDefault("false")
    boolean is_in_work;

    @ManyToOne
    @JoinColumn(name = "role_id", referencedColumnName = "id", nullable = false)
    private Role role;

    public User() {
    }

    public User(String name, String surname, String username, String password, Date data_of_birth, String address, Role role) {
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
        this.data_of_birth = data_of_birth;
        this.address = address;
        this.role = role;
    }

    public boolean getIs_in_work() {
        return is_in_work;
    }

    public void setIs_in_work(boolean is_in_work) {
        this.is_in_work = is_in_work;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getData_of_birth() {
        return data_of_birth;
    }

    public void setData_of_birth(Date data_of_birth) {
        this.data_of_birth = data_of_birth;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", data_of_birth=" + data_of_birth +
                ", address='" + address + '\'' +
                ", is_in_work='" + is_in_work + '\'' +
//                ", role=" + role.toString() +
                '}';
    }
}
