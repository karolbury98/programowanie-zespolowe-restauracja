package pl.wiickend;

import org.junit.jupiter.api.Test;
import pl.wiickend.restaurant.entity.Role;
import pl.wiickend.restaurant.entity.User;
import pl.wiickend.restaurant.Main;
import pl.wiickend.restaurant.services.entity.RoleService;
import pl.wiickend.restaurant.services.entity.UserService;
import pl.wiickend.restaurant.utils.Database;

import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

public class UserTest {

    @Test
    public void testCreateUser() throws Exception {
        Main.database = new Database();
        UserService userService = new UserService();
        java.util.Date date = new java.util.Date();
        java.sql.Date date1 = new java.sql.Date(date.getTime());
        RoleService roleService = new RoleService();
        Role role = roleService.getById(2);
        User user = new User("Jan","Kowalski","jkowalski123","jkowalski123",date1,"Warszawa",role);
        userService.save(user);

        List<User> listOfUsers = userService.loadAll();
        for(int i=0; i<listOfUsers.size(); i++) {
            boolean result;
            if(listOfUsers.get(i).getUsername().equals(user.getUsername())) {
                result = true;
                assertTrue(result);
            } else {
                result = false;
            }
        }

        userService.delete(user);
    }

    @Test
    public void testUpdateUser() throws Exception {
        Main.database = new Database();
        UserService userService = new UserService();
        java.util.Date date = new java.util.Date();
        java.sql.Date date1 = new java.sql.Date(date.getTime());
        RoleService roleService = new RoleService();
        Role role = roleService.getById(3);
        User user = new User("Jan","Nowak","jnowak123","jnowak123",date1,"Opole",role);
        userService.save(user);

        String newUsername = "jnowak321";
        user.setUsername(newUsername);
        userService.update(user);

        List<User> listOfUsers = userService.loadAll();

        for(int i=0; i<listOfUsers.size(); i++) {
            boolean result;
            if(listOfUsers.get(i).getUsername().equals(newUsername)) {
                result = true;
                assertTrue(result);
            } else {
                result = false;
            }
        }

        userService.delete(user);

    }

    @Test
    public void testDeleteUser() throws Exception {
        Main.database = new Database();
        UserService userService = new UserService();
        java.util.Date date = new java.util.Date();
        java.sql.Date date1 = new java.sql.Date(date.getTime());
        RoleService roleService = new RoleService();
        Role role = roleService.getById(1);
        User user = new User("Karol","Kowalski","karolkowalski","karolkowalski",date1,"Opole",role);
        userService.save(user);

        userService.delete(user);

        List<User> listOfUsersAfterDelete = userService.loadAll();
        boolean result = listOfUsersAfterDelete.contains(user.getUsername());
        assertFalse(result);

    }
}
