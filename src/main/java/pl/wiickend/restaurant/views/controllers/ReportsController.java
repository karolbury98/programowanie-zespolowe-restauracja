package pl.wiickend.restaurant.views.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import pl.wiickend.pdfgenerator.Report;
import pl.wiickend.restaurant.entity.Pdf;
import pl.wiickend.restaurant.reports.ProductsInWarehouseReport;
import pl.wiickend.restaurant.Main;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class ReportsController {

    @FXML
    private TableView<Pdf> filesTable;

    @FXML
    private TableColumn<Pdf, String> filenameColumn;

    @FXML
    private TableColumn<Pdf, String> dateColumn;

    @FXML
    private ComboBox<Report> reportsComboBox;

    @FXML
    public void generateReport(ActionEvent event) throws IOException {
        Report selectedItem = reportsComboBox.getSelectionModel().getSelectedItem();
        if(selectedItem != null) {

            selectedItem.createPdf(Main.loggedUser.getName() + " "+Main.loggedUser.getSurname());
            this.updateFiles();
        }
    }

    private void updateFiles() throws IOException {
        filesTable.getItems().clear();

        List<Pdf> pdfs = new ArrayList<>();
        File[] files = Report.getExistingPdfs(Main.PDF_DEST);

        for (File file :
                files) {

            BasicFileAttributes attrs = Files.readAttributes(file.toPath(), BasicFileAttributes.class);
            FileTime time = attrs.creationTime();

            String pattern = "yyyy-MM-dd HH:mm:ss";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

            String formatted = simpleDateFormat.format( new Date( time.toMillis() ) );

            pdfs.add(new Pdf(file.getName(), formatted, file.toString(), time.toMillis()));
        }

        pdfs.sort(new Comparator<Pdf>() {
            @Override
            public int compare(Pdf pdf1, Pdf pdf2) {
                return (int) (pdf2.getDateMilis() - pdf1.getDateMilis());
            }
        });

        filesTable.getItems().addAll(pdfs);
    }

    @FXML
    public void initialize() throws IOException {
        assert filesTable != null : "fx:id=\"filesTable\" was not injected: check your FXML file 'ReportsView.fxml'.";
        assert filenameColumn != null : "fx:id=\"filenameColumn\" was not injected: check your FXML file 'ReportsView.fxml'.";
        assert dateColumn != null : "fx:id=\"dateColumn\" was not injected: check your FXML file 'ReportsView.fxml'.";
        assert reportsComboBox != null : "fx:id=\"reportsComboBox\" was not injected: check your FXML file 'ReportsView.fxml'.";

        filenameColumn.setCellValueFactory(new PropertyValueFactory<Pdf, String>("name"));
        dateColumn.setCellValueFactory(new PropertyValueFactory<Pdf, String>("date"));

        ProductsInWarehouseReport productsInWarehouseReport = new ProductsInWarehouseReport();
        reportsComboBox.getItems().clear();
        reportsComboBox.getItems().addAll(productsInWarehouseReport);

        this.updateFiles();

        filesTable.setRowFactory(tv -> {
            TableRow<Pdf> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    Pdf pdf = row.getItem();
                    File pdfFile = new File(pdf.getPath());

                    new Thread(() -> {
                        try {
                            Desktop.getDesktop().browse(pdfFile.toURI());
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                    }).start();
                }
            });
            return row;
        });
    }
}

