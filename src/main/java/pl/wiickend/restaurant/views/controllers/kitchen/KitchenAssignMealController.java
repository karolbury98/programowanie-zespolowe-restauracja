package pl.wiickend.restaurant.views.controllers.kitchen;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import pl.wiickend.restaurant.entity.MealOrder;
import pl.wiickend.restaurant.entity.User;
import pl.wiickend.restaurant.services.entity.MealOrderService;
import pl.wiickend.restaurant.services.entity.MealProductService;
import pl.wiickend.restaurant.services.entity.ProductService;
import pl.wiickend.restaurant.services.entity.UserService;
import pl.wiickend.restaurant.utils.AlertAdder;

import java.net.URL;
import java.util.ResourceBundle;

public class KitchenAssignMealController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ListView<User> cheffList;

    @FXML
    private Text mealName;

    @FXML
    private Text mealAmount;

    @FXML
    private Text mealTime;

    MealProductService mealProductService = new MealProductService();

    @FXML
    void assignCheff(ActionEvent event) {
        if (cheffList.getSelectionModel().getSelectedItem() == null) {
            return;
        }
        try {
            mealOrderService.changeMealOrderStateToPreparation(mealOrder, cheffList.getSelectionModel().getSelectedItem());
        } catch (Exception exception) {
            AlertAdder alertError = new AlertAdder(Alert.AlertType.ERROR, "Error", exception.getMessage());
            return;
        }

        Stage stage = (Stage) mealName.getScene().getWindow();
        stage.close();
    }

    ProductService productService = new ProductService();


    private MealOrder mealOrder;

    public void setMealOrder(MealOrder mealOrder) {
        this.mealOrder = mealOrder;
        mealName.setText(mealOrder.getMealNameObj());
        mealAmount.setText(mealOrder.getQuantity().toString());
        mealTime.setText(mealOrder.getMealTime());
    }

    UserService userService = new UserService();
    MealOrderService mealOrderService = new MealOrderService();

    @FXML
    void initialize() {
        assert cheffList != null : "fx:id=\"cheffList\" was not injected: check your FXML file 'KitchenAssignMealView.fxml'.";
        assert mealName != null : "fx:id=\"mealName\" was not injected: check your FXML file 'KitchenAssignMealView.fxml'.";
        assert mealAmount != null : "fx:id=\"mealAmount\" was not injected: check your FXML file 'KitchenAssignMealView.fxml'.";
        assert mealTime != null : "fx:id=\"mealTime\" was not injected: check your FXML file 'KitchenAssignMealView.fxml'.";
        cheffList.setItems(FXCollections.observableList(userService.getChefsWithoutAssignedMeal()));
        cheffList.setCellFactory(l -> new ListCell<User>() {
            @Override
            protected void updateItem(User user, boolean b) {
                super.updateItem(user, b);
                if (b || user == null || user.getName() == null || user.getSurname() == null) {
                    setText("");
                } else {
                    setText(user.getName() + " " + user.getSurname());
                }
            }
        });
//        System.out.println(userService.getUsersWithRole("Chef"));
        System.out.println(cheffList.getItems());
    }
}
