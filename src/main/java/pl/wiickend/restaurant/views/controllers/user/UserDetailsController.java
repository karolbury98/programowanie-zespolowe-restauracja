package pl.wiickend.restaurant.views.controllers.user;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import pl.wiickend.restaurant.entity.User;

public class UserDetailsController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Text userName;

    @FXML
    private Text userSurname;

    @FXML
    private Text userUsername;

    @FXML
    private Text userPassword;

    @FXML
    private Text userDateOfBirth;

    @FXML
    private Text userAddress;

    @FXML
    private Text userRole;

    @FXML
    void close(ActionEvent event) {
        Stage stage = (Stage) userName.getScene().getWindow();
        stage.close();
    }

    @FXML
    void initialize() {
        assert userName != null : "fx:id=\"userName\" was not injected: check your FXML file 'UsersDetailsView.fxml'.";
        assert userSurname != null : "fx:id=\"userSurname\" was not injected: check your FXML file 'UsersDetailsView.fxml'.";
        assert userUsername != null : "fx:id=\"userUsername\" was not injected: check your FXML file 'UsersDetailsView.fxml'.";
        assert userPassword != null : "fx:id=\"userPassword\" was not injected: check your FXML file 'UsersDetailsView.fxml'.";
        assert userDateOfBirth != null : "fx:id=\"userDateOfBirth\" was not injected: check your FXML file 'UsersDetailsView.fxml'.";
        assert userAddress != null : "fx:id=\"userAddress\" was not injected: check your FXML file 'UsersDetailsView.fxml'.";
        assert userRole != null : "fx:id=\"userRole\" was not injected: check your FXML file 'UsersDetailsView.fxml'.";

    }

    public void setUser(User user) {
        userName.setText(user.getName());
        userUsername.setText(user.getUsername());
        userSurname.setText(user.getSurname());
        userAddress.setText(user.getAddress());
        userPassword.setText(user.getPassword());
        userDateOfBirth.setText(String.valueOf(user.getData_of_birth()));
        userRole.setText(String.valueOf(user.getRole()));
    }
}
