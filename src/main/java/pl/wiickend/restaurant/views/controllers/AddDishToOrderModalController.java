package pl.wiickend.restaurant.views.controllers;

import java.io.IOException;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import pl.wiickend.restaurant.entity.Meal;
import pl.wiickend.restaurant.entity.Order;
import pl.wiickend.restaurant.services.entity.MealOrderService;
import pl.wiickend.restaurant.services.entity.MealService;
import pl.wiickend.restaurant.services.entity.OrderService;

public class AddDishToOrderModalController {
    @FXML
    private TableView<Meal> dishesTable;

    @FXML
    private TableColumn<Meal, String> name_col;

    @FXML
    private TableColumn<Meal, Float> price_col;

    @FXML
    private TextField quantityField;

    @FXML
    private Button addDishButton;

    private Meal addedDish;

    public Meal getDish(){
        return addedDish;
    }

    public void setAddedDish(Meal addedDish) {
        this.addedDish = addedDish;
    }

    @FXML
    private Button cancelButton;
    private ObservableList<Meal> data;
    MealService mealService = new MealService();
    OrderService orderService = new OrderService();
    MealOrderService mealOrderService = new MealOrderService();
    Order selectedOrder;

    @FXML
    void addDish(ActionEvent event) throws IOException {
        if(selectedOrder != null){
        Meal newMeal = dishesTable.getSelectionModel().getSelectedItem();
        newMeal.setInOrderQuantity(Float.valueOf(quantityField.getText()));
        selectedOrder.addMeal(newMeal);
        orderService.update(selectedOrder);
        } else{
            Meal newMeal = dishesTable.getSelectionModel().getSelectedItem();
            newMeal.setInOrderQuantity(Float.valueOf(quantityField.getText()));
            setAddedDish(newMeal);
        }

        Stage stage = (Stage)addDishButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    void cancelButton(ActionEvent event) {
        Stage stage = (Stage)addDishButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    void initialize() {
        name_col.setCellValueFactory(new PropertyValueFactory<Meal, String>("name"));
        price_col.setCellValueFactory(new PropertyValueFactory<Meal, Float>("gross_price"));

        ObservableList<Meal> MealList = FXCollections.observableArrayList();
        List<Meal> eList = mealService.loadAll();
        for (Meal ent : eList) {
            MealList.add(ent);
        }
        data = MealList;
        dishesTable.setItems(data);
    }

    public void setOrder(Order selectedOrder) {
        this.selectedOrder = selectedOrder;
    }
}
