package pl.wiickend.restaurant;

import pl.wiickend.restaurant.entity.*;
import pl.wiickend.restaurant.services.SeederService;

public class Seeder {
    public static void main(String[] args) throws Exception {
        SeederService seederService = new SeederService();
        seederService.seed(Role.class);
        seederService.seed(User.class);
        seederService.seed(Product.class);
        seederService.seed(Meal.class);
        seederService.seed(Menu.class);
        seederService.seed(MealOrder.class);
    }
}
